
// Url-JSON
const URL1_ = "https://pruebas_madarme.gitlab.io/persistencia/pg_web/app_celulares_planes/personas_cedula_foto.json";
const URL2_  = "https://pruebas_madarme.gitlab.io/persistencia/pg_web/app_celulares_planes/llamadas.json";

async function leerJSON(url){
    try{
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch(err) {
        alert(err);
    }
}


function leerDatos(){
    leerJSON(URL1_).then(datos => {
        dibujarTabla1(datos);
       // dibujarTabla2(datos)
    });
    leerDatosJson2();
}

function leerDatosJson2(){
    leerJSON(URL2_).then(datos => {
        dibujarTabla2(datos)
    });
}

function dibujarTabla2(datos){
    let cedulaS = document.getElementById("cedula").value;
    let suscriptor = []
    var data = new google.visualization.DataTable();
    data.addColumn("number","CEDULA");
    data.addColumn("string", "celular_origen");
    data.addColumn("string", "Numero_Destino");
    data.addColumn("string", "T_Inicial");
    data.addColumn("string", "T_Final");
    data.addColumn("number", "T_consumido");
    data.addColumn("string", "Min_ConsumidoS");
    data.addColumn("string", "País");
    data.addColumn("string", "Valor_Min");
    data.addColumn("string", "Precio");

    let contador = 0;
    for (let i = 0; i < datos.length; i++) {
        if(parseInt(cedulaS) === datos[i].CEDULA){
            data.addRows(1);
            let tiempo_consumido =  tiempoConsumido(datos[i].tiempo_inicial, datos[i].tiempo_final);
            data.setCell(contador,0, datos[i].CEDULA + "");
            data.setCell(contador,1, datos[i].celular_origen + "");
            data.setCell(contador,2, datos[i].numero_telefonico_destino + "");
            data.setCell(contador,3, datos[i].tiempo_inicial + "");
            data.setCell(contador,4, datos[i].tiempo_final + "");
            data.setCell(contador,5, tiempo_consumido + "");
            contador++;
        }
    }
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
}

//resta por horas :(
function tiempoConsumido(inicial, final) {
    let tInicial = parseInt(inicial);
    let tFinal = parseInt(final);
    return tFinal - tInicial;
}

function validarCedula(cedula) {}

function dibujarTabla1(datos){
    document.getElementById("table").style = "display:block";
    let cedulaS = document.getElementById("cedula").value;
    let suscriptor = datos.filter(dato => parseInt(cedulaS) === dato.CEDULA)[0];
    document.getElementById("nombreT").textContent = suscriptor.Nombres;
    document.getElementById("emailT").textContent = suscriptor.Correo;
    document.getElementById("fotoT").innerHTML = `<img src=${suscriptor.URL_FOTO} style="width: 60px" >`
}

// function dibujarTabla2(datos){
//     let cedulaS = document.getElementById("cedula").value;
//     let suscriptor = datos.filter(dato => parseInt(cedulaS) === dato.CEDULA)[0];
//     var data = new google.visualization.DataTable();
//     data.addColumn("number","CEDULA");
//     data.addColumn("string", "celular_origen");
//     data.addColumn("string", "Numero_Destino");
//     data.addColumn("string", "T_Inicial");
//     data.addColumn("string", "T_Final");
//     data.addColumn("string", "T_consumido");
//     data.addColumn("string", "Min_ConsumidoS");
//     data.addColumn("string", "País");
//     data.addColumn("string", "Valor_Min");
//     data.addColumn("string", "Precio");
//     data.addRows(datos.length);
//
//     for (let i = 0; i < datos.length; i++) {
//         data.setCell(i,0, suscriptor.CEDULA + "");
//         // data.setCell(i,1, nombre);
//         // data.setCell(i,2, numero);
//
//     }
//     var table = new google.visualization.Table(document.getElementById('table_div'));
//     table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
// }

// function dibujarPie() {
//     var data = google.visualization.arrayToDataTable([
//         ["Estado","Porcentaje"],
//         ["Aprobado",50],
//         ["Desaprobado",50]
//     ]);
//     var options = {
//         title: '',
//         is3D: true,
//     };
//     var chart = new google.visualization.PieChart(document.getElementById('pie_div'));
//     chart.draw(data, options);
// }
//
// function graficarFuncion(){
//     var data = new google.visualization.DataTable();
//     data.addColumn('number', 'Mes');
//     data.addColumn('number', 'Valor');
//     data.addRows(5);
//
//     for (let i = 0; i < 5; i++) {
//         data.setCell(i,0, i+1);
//         data.setCell(i,1, i);
//     }
//
//     var options = {
//         chart: {
//             title: 'Gráfica de ejemplo'
//         },
//         width: 900,
//         height: 500
//     };
//
//     var chart = new google.charts.Line(document.getElementById('line-chart_div'));
//
//     chart.draw(data, google.charts.Line.convertOptions(options));
// }